const squareSize = 3;
const gameDelay = 0;
const screenSizeModifier = 0.99;
const iHeight = (top.innerWidth / squareSize) * screenSizeModifier;
const jWidth = (top.innerWidth / squareSize) * screenSizeModifier;
const startingDensity = 0.4;
const pixelColor = "white";
const boardColor = "black";

var myGameArea = {
    canvas: document.createElement("canvas"),
    start: function () {
        this.canvas.width = top.innerWidth * screenSizeModifier;
        this.canvas.height = top.innerHeight * screenSizeModifier;
        this.canvas.id = "canvas";
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
    }
}

var boardA = [];
var boardB = [];

function initializeBoard() {
    for (let i = 0; i < iHeight; i++) {
        let rowA = [];
        let rowB = [];
        for (let j = 0; j < jWidth; j++) {

            if (Math.random() > startingDensity) {
                rowA[j] = true;
            }
            else {
                rowA[j] = false;
            }
            rowB[j] = false;
        }
        boardA.push(rowA);
        boardB.push(rowB);
    }
}

async function playNewerGame() {

    initializeBoard();
    myGameArea.start();

    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");

    renderInitialGameBoard(boardB, boardA, ctx);

    while (true) {
        var kobe = sleep(gameDelay)
        calculateNextBoardstate(boardA, boardB);
        renderGameBoard(boardA, boardB, ctx);
        await kobe;

        var yeet = sleep(gameDelay);
        calculateNextBoardstate(boardB, boardA);
        renderGameBoard(boardB, boardA, ctx);
        await yeet;
    }
}

function renderInitialGameBoard(oldBoard, newBoard, ctx) {
    for (let i = newBoard.length - 1; i >= 0; i--) {
        for (let j = newBoard[i].length - 1; j >= 0; j--) {
            ctx.fillStyle = newBoard[i][j] ? pixelColor : boardColor;
            ctx.fillRect(i * squareSize, j * squareSize, squareSize, squareSize);
        }
    }
}

function renderGameBoard(oldBoard, newBoard, ctx) {

    for (let i = newBoard.length - 1; i >= 0; i--) {

        for (let j = newBoard[i].length - 1; j >= 0; j--) {

            if (oldBoard[i][j] != newBoard[i][j]) {
                ctx.fillStyle = newBoard[i][j] ? pixelColor : boardColor;
                ctx.fillRect(i * squareSize, j * squareSize, squareSize, squareSize);
            }
        }
    }
}

function calculateNextBoardstate(oldBoard, newBoard) {
    for (let i = oldBoard.length - 1; i >= 0; i--) {
        for (let j = oldBoard[i].length - 1; j >= 0; j--) {

            let count = calculateNeighbor(oldBoard, i, j);

            newBoard[i][j] = (count == 3) || (count == 2 && oldBoard[i][j]);
        }
    }
}

function calculateNeighbor(board, i, j) {
    let result = 0;

    try { result += board[i - 1][j]; } catch{ }
    try { result += board[i - 1][j + 1]; } catch{ }
    try { result += board[i - 1][j - 1]; } catch{ }
    try { result += board[i + 1][j]; } catch{ }
    try { result += board[i + 1][j + 1]; } catch{ }
    try { result += board[i + 1][j - 1]; } catch{ }
    try { result += board[i][j + 1]; } catch{ }
    try { result += board[i][j - 1]; } catch{ }

    return result;
}

const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

playNewerGame();
